package com.example.prostykalkulator;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Build;

public class KalkulatorActivity extends Activity {
	
    private String liczba="";
    private EditText pole_liczbowe;
    
    private boolean decSep = false;
    private double pLiczba = 0.0;
    private double dLiczba = 0.0;
    private boolean dzialanie = false;
    private boolean mWybranoDzialanie = false;
    private boolean drugaLiczba = false;
    
    private String typDzialania=""; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kalkulator);

		pole_liczbowe = (EditText) this.findViewById(R.id.editText1);
		
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.kalkulator, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void initEditText()
	{
		pole_liczbowe = (EditText) this.findViewById(R.id.editText1);
	}
	
	private void wybranoDzialanie()
	{
		Button dod = (Button) this.findViewById(R.id.button_dod);
		Button ode = (Button) this.findViewById(R.id.button_ode);
		Button mnoz= (Button) this.findViewById(R.id.button_mnoz);
		Button dziel= (Button) this.findViewById(R.id.button_dziel);
		
		if(mWybranoDzialanie)
		{
			dod.setEnabled(false);
			ode.setEnabled(false);
			mnoz.setEnabled(false);
			dziel.setEnabled(false);
		}
		else
		{
			dod.setEnabled(true);
			ode.setEnabled(true);
			mnoz.setEnabled(true);
			dziel.setEnabled(true);
		}
	}

	public void onLiczbaClick(View view) {
		initEditText();
		
		String nazwa = "";
		Button przycisk = (Button)this.findViewById(view.getId());

		if(!przycisk.equals(null))
		{
				nazwa = przycisk.getText().toString();
				liczba += nazwa;
				pole_liczbowe.setText(liczba);
		}
	}

	public void onDecSepClick(View view) {
		initEditText();
		
		Button zrodlo = (Button) this.findViewById(view.getId());

		String nazwa = zrodlo.getText().toString();

		if (decSep == false) {
			liczba += nazwa;
			pole_liczbowe.setText(liczba);
		}

		decSep = true;
	}

	public void onCButtonClick(View view) {
		initEditText();
		
		pole_liczbowe.setText("");
		liczba = "";
		drugaLiczba = false;
		decSep = false;
		dzialanie = false;
	}

	public void onDzialanieClick(View view) {
		Button przycisk = (Button) this.findViewById(view.getId());
		String t_dzialanie = przycisk.getText().toString();
		
		if (t_dzialanie.equals("-") && liczba.equals("")) {
			dzialanie = false;
			initEditText();
			
			liczba += t_dzialanie;
			pole_liczbowe.setText(liczba);
		} else {
			dzialanie = true;
		}
		 

		if (dzialanie) {
			mWybranoDzialanie=true;
			wybranoDzialanie();
			pLiczba = Double.parseDouble(liczba);

			liczba = "";
			pole_liczbowe.setText(liczba);

			typDzialania = t_dzialanie;
			drugaLiczba = true;
			decSep = false;
		}
	}

	public void onEquals(View view) {
		if (drugaLiczba) {
			try {
				dLiczba = Double.parseDouble(liczba);
			} catch (NumberFormatException e) {
				dLiczba = 0.0;
			}
		}

		double wynik = 0.0;

		if (typDzialania.equals("+")) {
			wynik = pLiczba + dLiczba;
		}

		if (typDzialania.equals("-")) {
			wynik = pLiczba - dLiczba;
		}

		if (typDzialania.equals("*")) {
			wynik = pLiczba * dLiczba;
		}

		if (typDzialania.equals("/")) {
			if (dLiczba != 0) {
				wynik = pLiczba / dLiczba;
			} else {
				Toast.makeText(getApplicationContext(),
						"Nie można dzielić przez 0", Toast.LENGTH_SHORT).show();
				wynik = 0;
			}
		}

		drugaLiczba = false;
		pole_liczbowe.setText(String.format("%g", wynik));
		mWybranoDzialanie = false;
		wybranoDzialanie();
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_kalkulator,
					container, false);
			return rootView;
		}
	}

}
